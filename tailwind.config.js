/** @type {import('tailwindcss').Config} */
const config = {
  darkMode: 'class',
  content: ['./src/**/*.{html,js,svelte,ts}'],
  safelist: [],
  theme: {
    extend: {
    }
  },
  plugins: [],
  rules: {
    // require() Require statement not part of import statement.
    '@typescript-eslint/no-var-requires': 0
  }
}

// module.exports = config
export default config
