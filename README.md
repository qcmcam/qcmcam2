QCMCam 2 est un projet de réécriture de https://qcmcam.net

QCMCam est un outil de sondage par QCM et carton de vote.

## Pourquoi une deuxième version ?
QCMCam en se voulant efficace n'est pas tout à fait ergonomique : toutes les commandes d'édition et d'utilisation sont visibles à l'usage et il est parfois compliqué de s'y retrouver dans les différents boutons.
Nous allons travailler à scinder les usages :
- édition des QCM
- édition des listes de participants
- passation des QCM
- analyse des résultats

Nous travaillerons également à permettre aux usager d'enregistrer plus facilement leurs QCM et à ajouter des outils de gestion et gamification de classe.

De plus, nous travaillerons à améliorer la documentation des fichiers pour aider les éventuels co auteurs de cet outil.

## Licence
QCMCam 2 est sous licence Apache 2.0

# Développement
Pour le tester :
- se rendre sur https://q2.qcmcam.net

Pour développer :
- cloner le dépôt
- `pnpm i` pour installer les dépendances
- `pnpm start` pour démarrer le serveur local

Pour le déploiement :
- `pnpm build` va créer les fichiers dans le dossier dist/
- uploader les fichiers du dossier `dist/` sur le serveur de production
