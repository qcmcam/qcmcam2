import {
  InlineEditor as InlineEditorBase,
  Essentials,
  Alignment,
  Autoformat,
  Bold,
  Italic,
  Code,
  BlockQuote,
  Base64UploadAdapter,
  Font,
  Heading,
  Image,
  ImageBlock,
  ImageCaption,
  ImageStyle,
  ImageToolbar,
  ImageUpload,
  ImageResize,
  Indent,
  Link,
  List,
  MediaEmbed,
  Paragraph,
  PasteFromOffice,
  PictureEditing,
  Table,
  TableToolbar,
  TextTransformation
} from 'ckeditor5'

import Math from '@isaul32/ckeditor5-math/src/math.js'
import AutoformatMath from '@isaul32/ckeditor5-math/src/autoformatmath.js'

export default class InlineEditor extends InlineEditorBase {
  public static override builtinPlugins = [
    Essentials,
    Alignment,
    Autoformat,
    Bold,
    Italic,
    Code,
    BlockQuote,
    Base64UploadAdapter,
    Font,
    Heading,
    Image,
    ImageBlock,
    ImageCaption,
    ImageStyle,
    ImageToolbar,
    ImageUpload,
    ImageResize,
    Indent,
    Link,
    List,
    MediaEmbed,
    Paragraph,
    PasteFromOffice,
    PictureEditing,
    Table,
    TableToolbar,
    TextTransformation,
    Math,
    AutoformatMath
  ]

  public static override defaultConfig = {
    autoParagraph: false,
    toolbar: {
      items: [
        'undo', 'redo',
        '|', 'heading',
        '|', 'bold', 'italic', 'code',
        '|', 'fontFamily', 'fontColor', 'fontBackgroundColor',
        '|', 'link', 'insertTable', 'blockQuote', 'mediaEmbed',
        '|', 'bulletedList', 'numberedList', 'outdent', 'indent', 'alignment',
        '|', 'math'
      ]
    },
    image: {
      toolbar: [
        'imageStyle:inline',
        {
          // Grouping the buttons for the icon-like image styling
          // into one drop-down.
          name: 'imageStyle:icons',
          title: 'Alignment',
          items: [
            'imageStyle:alignLeft',
            'imageStyle:alignRight',
          ],
          defaultItem: 'imageStyle:alignLeft'
        }, {
          // Grouping the buttons for the regular
          // picture-like image styling into one drop-down.
          name: 'imageStyle:pictures',
          title: 'Style',
          items: ['imageStyle:alignBlockLeft', 'imageStyle:alignCenter', 'imageStyle:alignBlockRight'],
          defaultItem: 'imageStyle:alignCenter'
        },
        'toggleImageCaption',
        'imageTextAlternative'
      ]
    },
    table: {
      contentToolbar: [
        'tableColumn',
        'tableRow',
        'mergeTableCells'
      ]
    },
    math: {
      engine: 'katex',
      outputType: 'span',
      className: 'math-tex'
    },
    // This value must be kept in sync with the language defined in webpack.config.js.
    language: 'fr'
  }
}
