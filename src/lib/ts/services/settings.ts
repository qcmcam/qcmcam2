import { get } from 'svelte/store'
import storage from './storage'
import { config } from './store'

export function saveSettings () {
  if (storage.isAvailable()) {
    storage.db.store_settings.put({ uid: 'config', data: get(config) })
  }
}
