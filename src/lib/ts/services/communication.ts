import language from './language.js'
import utils from './utils.js'

declare const Peer: any

function showQRCode (peerId: string): void {
  // do nothing now
}

function peerConnected () {
  utils.DOM.addClass('btnportable', 'is-hidden')
  utils.DOM.removeClass('btnstopportable', 'is-hidden')
}
function connectedMessage () {
  utils.DOM.text('cameras-list', language.dictionary.messages.connexionestablished)
  utils.DOM.removeClass('qrious', 'montre')
  utils.DOM.addClass('qrious', 'cache')
  // on enlève le cache gris des question si présent
  // diaporama.toggleCacheQuestions(true)
}
export const communication = {
  type: 'peerjs', // ou 'php'
  peerId: '',
  sessionId: '',
  intervalSession: 0,
  interval: 0,
  stopSessionRequest: false,
  peer: new Peer(),
  pile: [],
  start: () => {
    communication.peer = new Peer()
    communication.peer.on('open', function (id: string) {
      communication.peerId = id
      showQRCode(communication.peerId)
      communication.peer.on('connection', function (conn: any) {
        peerConnected()
        const peerConnexion = conn
        peerConnexion.on('data', function (data: any) {
          // usage des datas reçus
          // premier envoi : confirmation et envoi des infos de based
          if (data.connexion === 'paired') {
            connectedMessage()
            // confirmation de la connexion, on envoi les premiers datas
            const datas = communication.initialDatas()
            peerConnexion.send(datas)
          }
          // lecture des données...
          communication.traiteJSON(data)
        })
        function checkState (): void {
          const json = communication.updateState()
          answers.tests(false)
          if (!utils.isEmpty(json)) {
            peerConnexion.send(json)
          }
        }
        communication.interval = setInterval(checkState, 1633)
      })
    })
    communication.peer.on('error', function (err: string) {
      // peer = undefined;
      // en cas d'impossibilité d'utiliser peerjs, on utilise ajax
      if (err === 'browser-incompatible') {
        communication.loadSessionPhp()
      } else {
        utils.DOM.text('cameras-list', 'Erreur de connexion peerJS : ' + err)
      }
    })
    communication.peer.on('disconnected', function () {
      utils.DOM.text('cameras-list', language.dictionary.messages.deconnectionSmartphone)
      if (communication.interval > 0) { clearInterval(communication.interval) }
      if (communication.peerId !== '') {
        console.log('Déconnexion, tentative de reconnexion')
        communication.peer.id = communication.peerId
        communication.peer._lastServerId = communication.peerId
        communication.peer.reconnect()
      }
    })
    communication.peer.on('close', function () {
      if (communication.interval > 0) { clearInterval(communication.interval) }
      communication.peerId = ''
      communication.peerConnexion = null
      utils.DOM.text('cameras-list', language.dictionary.messages.connexionclosed)
    })
  },
  loadSessionPhp: function () {
    const reader = new XMLHttpRequest()
    reader.onload = function () {
      const rep = JSON.parse(reader.responseText)
      if (rep.connexion === 'failed') {
        utils.DOM.text('lienqr', '<span class="text-red">' + language.dictionary.messages.connexionfailed + '</span>')
        return false
      }
      communication.sessionId = rep.id
      utils.DOM.class('btnportable', 'w3-hide')
      utils.DOM.class('btnstopportable', '')
      communication.deporterScan()
    }
    reader.open('get', 'sessionapp.php', true)
    reader.send()
  },
  stop: function () {
    if (communication.peerId !== '') {
      communication.peerConnexion.send({
        commande: 'stop'
      })
      communication.peer.destroy()
    } else {
      communication.stopSessionRequest = true
    }
    utils.DOM.class('btnportable', '')
    utils.DOM.class('btnstopportable', 'w3-hide')
  },
  getSessionPhpData: function () {
    const json = communication.updateState()
    const reader = new XMLHttpRequest()
    reader.onload = function () {
      if (reader.responseText !== '') {
        // on sépare les lignes de réponse
        const lignesjson = reader.responseText.split('\n')
        for (let i = 0; i < lignesjson.length; i++) {
          if (lignesjson[i] !== '') {
            communication.traiteJSON(lignesjson[i])
          }
        }
      }
      // en cas de demande d'arrêt de la session on arrête l'échange de données avec le serveur
      if (communication.stopSessionRequest) {
        communication.stopSessionRequest = false
        communication.intervalSession = 0
        return
      }
      communication.intervalSession = setTimeout(communication.getSessionData, 2333)
    }
    reader.open('get', 'sessionapp.php?s=' + communication.sessionId + '&json=' + JSON.stringify(json), true)
    reader.send()
  },
  updateState () {

  },
  getSessionData () {

  }
}
